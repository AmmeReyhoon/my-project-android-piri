package reyhanehpiri.myapplication;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Reyhaneh on 1/31/2018.
 */

public class BaseActivity extends AppCompatActivity {
    public Context mContext=this;
    public Activity mActivity=this;
}


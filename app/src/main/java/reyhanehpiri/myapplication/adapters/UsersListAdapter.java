package reyhanehpiri.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import reyhanehpiri.myapplication.PublicMethods;
import reyhanehpiri.myapplication.R;
import reyhanehpiri.myapplication.webmodels.UserPojoModel;

/**
 * Created by Reyhaneh on 1/31/2018.
 */

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.MyHolder> implements View.OnClickListener {
    private Context mContext;
    private List<UserPojoModel> users;

    public UsersListAdapter(Context mContext, List<UserPojoModel> users) {
        this.mContext = mContext;
        this.users = users;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);

        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UsersListAdapter.MyHolder holder, int position) {
        holder.name.setText(users.get(position).getName());
        holder.family.setText(users.get(position).getFamily());
        holder.age.setText(users.get(position).getAge().toString());

    }

    public void addItems(List<UserPojoModel> newUsers){
        users.addAll(newUsers);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView family;
        TextView age;

        public MyHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            family=itemView.findViewById(R.id.family);
            age=itemView.findViewById(R.id.age);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PublicMethods.showToast(mContext,users.get(getAdapterPosition()).getName());
                }
            });

        }
    }
}

package reyhanehpiri.myapplication;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import reyhanehpiri.myapplication.adapters.UsersListAdapter;
import reyhanehpiri.myapplication.webmodels.UserPojoModel;


public class MainActivity extends BaseActivity {
    RecyclerView list;
    private UsersListAdapter adapter;
    SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);
        swipe = findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
        findViewById(R.id.getData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
            }
        });
    }

    void getData() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constants.userURL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.showToast(mContext, throwable.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                List<UserPojoModel> users = getUsers(responseString);
                PublicMethods.showToast(mContext, users.get(0).getName());
                if (adapter != null) {
                    adapter.addItems(users);
                } else {
                    adapter = new UsersListAdapter(mContext, users);
                }
                list.setAdapter(adapter);
                RecyclerView.LayoutManager lm = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                list.setLayoutManager(lm);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                swipe.setRefreshing(false);
            }
        });
    }


    public List<UserPojoModel> getUsers(String jsonStr) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<UserPojoModel>>() {
        }.getType();
        return gson.fromJson(jsonStr, listType);
    }

}

package reyhanehpiri.myapplication;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Reyhaneh on 1/31/2018.
 */

public class PublicMethods {
    public static void showToast(Context mContext,String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
